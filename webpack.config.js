// Imports
const path = require('path');
const ESLintPlugin = require('eslint-webpack-plugin');

// Paths
const src = path.resolve(__dirname, 'src');
const dist = path.resolve(__dirname, 'dist');

module.exports = {
  mode: 'development',

  // Entry of our app
  entry: `${src}/index.js`,

  // Output path and filename
  output: {
    path: dist,
    filename: 'index.js',
    library: 'AnimateScrollTo',
    libraryTarget: 'umd',
  },

  // Add modules
  module: {
    rules: [
      {
        test: /\.js$/,
        include: src,
        loader: 'babel-loader',
      },
    ],
  },

  // Standard part for img imports
  resolve: {
    modules: [
      path.resolve(__dirname),
      'node_modules',
    ],
  },

  stats: {
    children: false,
  },

  plugins: [
    new ESLintPlugin({
      extensions: ['.js', '.jsx'],
    }),
  ]
};
