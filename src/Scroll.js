export default class Scroll {
  /**
   * Scroll to a particular location
   *
   * @param to       int Pixels to scroll to
   * @param duration int How long scroll must take in ms
   */
  constructor(to, duration) {
    this.elapsed = 0;
    this.start = window.pageYOffset;
    this.change = to - this.start;
    this.duration = duration;
    this.increment = 20;
  }

  /**
   * Animate scrolling
   */
  animateScroll() {
    this.elapsed += this.increment;

    // Get new position
    const position = this.easeInOut();

    // Scroll to new position
    window.scrollTo(0, position);

    // Rerun function if position is not yet correct
    if (this.elapsed < this.duration) {
      setTimeout(() => this.animateScroll(), this.increment);
    }
  }

  /**
   * easeInOut animation for scrolling
   */
  easeInOut() {
    let { elapsed } = this;
    const { start, change, duration } = this;

    elapsed /= duration / 2;

    if (elapsed < 1) {
      return ((change / 2) * elapsed * elapsed) + start;
    }

    elapsed -= 1;

    return ((-change / 2) * ((elapsed * (elapsed - 2)) - 1)) + start;
  }
}
