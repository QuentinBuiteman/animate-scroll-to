import Scroll from './Scroll';

const animateScrollTo = (to, duration) => {
  const scroll = new Scroll(to, duration);

  scroll.animateScroll();
};

export default animateScrollTo;
