# Animate Scroll To
Nicely and smoothly animate a scrollTo action.

## Installation
Using NPM:

```
npm install animate-scroll-to --save
```

Using Yarn:

```
yarn add animate-scroll-to
```

## How to use
Use the helper function provided by the library.

### animateScrollTo
Used to set the scroll position when a route is mounted.

```js
import animateScrollTo from 'animate-scroll-to';
```

Accepts two arguments:

* The pixel value to scroll to.
* The duration of the animation in ms.

```js
// Scroll back to top of the page in 1000ms.
animateScrollTo(0, 1000);
```
